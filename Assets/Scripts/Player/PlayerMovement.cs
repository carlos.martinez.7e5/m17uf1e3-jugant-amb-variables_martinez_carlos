using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    //Varaibles usadas para la animaci�n
    private bool _animate;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Sprite[] _animationSprites;
    [SerializeField] private double _framesPerSprite;
    private int _frame;
    private int _count;

    //Variables usadas para el movimiento automatico
    [SerializeField] private PlayerData _player;
    [SerializeField] private PlayerStates _currentState;
    [SerializeField] private double _secondsWaiting;
    private double _time;

    //Speed por slider
    [SerializeField] private GameObject _slider;
    private Slider _speedSlider;
    private double _originalSpeed;
    private bool _moveBySlider;

    // Start is called before the first frame update
    void Start()
    {
        _currentState = PlayerStates.WalkingL;
        _speedSlider = _slider.GetComponent<Slider>();
        _originalSpeed = _player.Speed;
        _moveBySlider = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (_moveBySlider == true)
            {
                _moveBySlider = false;
                _player.Speed = _originalSpeed;
            }
            else _moveBySlider = true;
        }

        if(_moveBySlider)_player.Speed = _speedSlider.value;

        if (_player.Height < 1) _player.Height = 1; //Para evitar errores en el editor al borrar el valor para poner otro
        _player.Height = _player.transform.localScale.x;

        if (_player.Speed == 0)  {
            _animate = false;
            _spriteRenderer.sprite = _animationSprites[0]; 
        }

        SwitchToManual();
        MovementAnimation();

        switch (_currentState)
        {
            case PlayerStates.WalkingL:
                MoveL();
                break;
            case PlayerStates.WalkingR:
                MoveR();
                break;
            case PlayerStates.Waiting:
                Wait();
                break;
            case PlayerStates.Manual:
                ManualMovement();
                break;
        }
    }

    public void MovementAnimation()
    {
        if (_animate)
        {
            _spriteRenderer.sprite = _animationSprites[_frame];
            _count++;

            //Cuanto m�s grande sea Petunia m�s lento se animar� || Deber�a ser cuanto m�s lento se mueva, m�s lento se animar�
            
            if (_count > _framesPerSprite * _player.transform.localScale.x) 
            {
                _count = 0;
                _frame++;
            }

            if (_frame > _animationSprites.Length - 1) _frame = 0;
        }
    }

    public enum PlayerStates
    {
        WalkingR,
        WalkingL,
        Waiting,
        Manual //No hace nada, para que el jugador se pueda mover libremente
    }

    public void MoveR()
    {
        _spriteRenderer.flipX = true;
        _animate = true;
        transform.position += new Vector3(((float)_player.Speed * Time.deltaTime) / (float)_player.Height, 0, 0);
        if (transform.position.x > _player.Distance) _currentState = PlayerStates.Waiting;
    }

    public void MoveL()
    {
        _animate = true;
        transform.position += new Vector3(((float)-(_player.Speed) * Time.deltaTime) / (float)_player.Height, 0, 0);
        if (transform.position.x < -(_player.Distance)) _currentState = PlayerStates.WalkingR;  
    }

    public void Wait()
    {
        _spriteRenderer.flipX = false;
        _spriteRenderer.sprite = _animationSprites[0];
        _animate = false;

        if (_time < _secondsWaiting) _time += Time.deltaTime;
        else
        {
            _currentState = PlayerStates.WalkingL;
            _time = 0;
        }
    }

    public void SwitchToManual()
    {
        if (Input.GetKey(KeyCode.M))
        {
            _currentState = PlayerStates.Manual;

            _animate = false;
            _spriteRenderer.sprite = _animationSprites[0];
        }  
    }

    public void ManualMovement()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            _animate = true;

            if (Input.GetAxis("Horizontal") > 0) _spriteRenderer.flipX = true;
            else if(Input.GetAxis("Horizontal") < 0) _spriteRenderer.flipX = false;
        }
        else
        {
            _animate = false;
            _spriteRenderer.sprite = _animationSprites[0];
        }
        transform.position += new Vector3(Input.GetAxis("Horizontal") * ((float)_player.Speed * Time.deltaTime) / (float)_player.Height, 0, 0);
    }
}
