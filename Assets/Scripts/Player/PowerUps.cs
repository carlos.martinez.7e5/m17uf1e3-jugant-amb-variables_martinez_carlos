using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{

    /*TO DO:
    -Popup con los controles (M para manual, S para slider, Space para hacer grande)
    */

    [SerializeField] private PlayerData _player;
    [SerializeField] private double _sizeMultiplier;
    private double _originalSize;
    [SerializeField] private double _growingSpeed;
    [SerializeField] private double _timeBeingGiant;
    private bool _beginCharge;
    private double _giantTime;
    [SerializeField] private double _giantCooldown;
    private bool _beginGiant;
    private double _chargeTime;
    [SerializeField] private GrowingStates _grow;
   
    // Start is called before the first frame update
    void Start()
    {
        _grow = GrowingStates.Nothing;
        _originalSize = _player.transform.localScale.x;
        _beginGiant = true;
        _beginCharge = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_sizeMultiplier < 1) _sizeMultiplier = 1;

        switch (_grow)
        {
            case GrowingStates.GrowingUp:
                GrowUp();
                break;
            case GrowingStates.GrowingDown:
                GrowDown();
                break;
            case GrowingStates.BeeingGiant:
                BeGiant();
                break;
            case GrowingStates.Charging:
                ChargeHability();
                break;
            case GrowingStates.Nothing:
                break;
        }

        if (Input.GetKeyDown(KeyCode.G) && _grow == GrowingStates.Nothing) _grow = GrowingStates.GrowingUp;
    }

    private enum GrowingStates
    {
        Charging,
        Nothing,
        GrowingUp,
        BeeingGiant,
        GrowingDown
    }

    private void GrowUp()
    {
        if (_player.transform.localScale.x < _originalSize * _sizeMultiplier)
        {
            _player.transform.localScale += new Vector3((float)_growingSpeed * Time.deltaTime,
                (float)_growingSpeed * Time.deltaTime,
                0);
        }
        else _grow = GrowingStates.BeeingGiant;
    }

    private void GrowDown()
    {
        if (_player.transform.localScale.x > _originalSize)
        {
            _player.transform.localScale += new Vector3(-(float)_growingSpeed * Time.deltaTime,
                -(float)_growingSpeed * Time.deltaTime,
                0);
        }
        else _grow = GrowingStates.Charging;
    }

    private void BeGiant()
    {
        if (_beginGiant)
        {
            _giantTime = Time.time + _timeBeingGiant;
            _beginGiant = false;
        }

        if (Time.time > _giantTime)
        {
            _grow = GrowingStates.GrowingDown;
            _beginGiant = true;
        }
    }

    private void ChargeHability()
    {
        if (_beginCharge)
        {
            _chargeTime = Time.time + _giantCooldown;
            _beginCharge = false;
        }

        if (Time.time > _chargeTime)
        {
            _grow = GrowingStates.Nothing;
            _beginGiant = true;
        }
    }
}
