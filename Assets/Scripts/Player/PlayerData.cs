using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public string PlayerName;
    public PlayerType PType;
    public double Height; //altura (es la X del sprite, reduce la velocidad)
    public double Speed;
    public double Distance;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("NameHolder") == null) PlayerName = "Petunia";
        else PlayerName = GameObject.Find("NameHolder").GetComponent<NameHolder>()._playerName;
    }
    // Update is called once per frame
    void Update()
    {
        //transform.localScale = new Vector3((float)this.Height, (float)this.Height, 0);
    }

    public enum PlayerType
    {
        Fighter,
        Mage,
        Necromancer,
        Ranger,
        Swordswoman
    }
}
