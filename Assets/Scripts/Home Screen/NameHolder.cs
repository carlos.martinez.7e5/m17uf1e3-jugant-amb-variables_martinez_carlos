using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NameHolder : MonoBehaviour
{

    public string _playerName;
    [SerializeField] private InputField _inputField;
    [SerializeField] private Button _startButton;
    
    // Start is called before the first frame update
    void Start()
    {
        _startButton.onClick.AddListener(SendName);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SendName()
    {
        _playerName = _inputField.text;
        DontDestroyOnLoad(this);
        SceneManager.LoadScene("MainScene");
    }
}
