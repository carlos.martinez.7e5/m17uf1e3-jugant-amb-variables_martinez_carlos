using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPlayerName : MonoBehaviour
{
    [SerializeField] private Text _guiName;
    [SerializeField] private PlayerData _player;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("NameHolder") == null) _guiName.text = "Petunia";
        else _guiName.text = GameObject.Find("NameHolder").GetComponent<NameHolder>()._playerName;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
