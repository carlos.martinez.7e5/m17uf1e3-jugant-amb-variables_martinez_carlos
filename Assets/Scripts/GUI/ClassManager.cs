using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassManager : MonoBehaviour
{

    [SerializeField] private Sprite[] _clasesImages;
    [SerializeField] private GameObject _currentClass;
    private Image _currentClassImg;
    [SerializeField] private Text _classTxt;

    private GameObject _player;
    private PlayerData _pData;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _pData = _player.GetComponent<PlayerData>();

        
        _currentClassImg = _currentClass.GetComponent<Image>();
        //_currentClassImg.sprite = _clasesImages[1];
    }

    // Update is called once per frame
    void Update()
    {
        _classTxt.text = (_pData.PType).ToString();
        UpdateClass();
    }

    private void UpdateClass()
    {
        switch (_pData.PType)
        {
            case PlayerData.PlayerType.Fighter:
                _currentClassImg.sprite = _clasesImages[0];
                break;
            case PlayerData.PlayerType.Mage:
                _currentClassImg.sprite = _clasesImages[1];
                break;
            case PlayerData.PlayerType.Necromancer:
                _currentClassImg.sprite = _clasesImages[2];
                break;
            case PlayerData.PlayerType.Ranger:
                _currentClassImg.sprite = _clasesImages[3];
                break;
            case PlayerData.PlayerType.Swordswoman:
                _currentClassImg.sprite = _clasesImages[4];
                break;
        }
    }
}
