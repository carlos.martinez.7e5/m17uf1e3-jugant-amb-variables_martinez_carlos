using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameRate : MonoBehaviour
{
    [SerializeField] private Text _fps;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FramesPerSecondCounter();
    }

    public void FramesPerSecondCounter()
    {
        _fps.text = Math.Round((1f / Time.deltaTime), 0).ToString();
    }
}
